import { CyberbitTaskPage } from './app.po';

describe('cyberbit-task App', () => {
  let page: CyberbitTaskPage;

  beforeEach(() => {
    page = new CyberbitTaskPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
