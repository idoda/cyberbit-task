import {Component, OnInit} from '@angular/core';
import {DataService} from './data.service';
import {Device} from "./device";
import {Time} from './time';
import {Protocol} from './protocol';
import {DeviceGroup} from './device-group';

@Component({
  selector: 'app-root',
  template: `
    <div class="row">
      <div class="col-md-3 col-sm-6">
        <div class="col-title"><span class="bold">1</span> Select Blackboxes</div>
        <app-device-groups *ngIf="deviceGroups" (devicesCheckedChangedEvent)="onDevicesCheckedChangedEvent($event)" 
                           [deviceGroups]="deviceGroups"></app-device-groups>
      </div>
      <div class="col-md-3 col-sm-6">
        <div class="col-title"><span class="bold">2</span> Select Protocols</div>
      <app-protocols (protocolsChanged)="onProtocolsChanged($event)" [protocols]="protocols"></app-protocols>
      </div>
      <div class="col-md-3 col-sm-6">
        <div class="col-title"><span class="bold">3</span> Select Time Period</div>
        <app-time-periods (radioChangedEvent)="onRadioChangedEvent($event)" [times]="timePeriods"></app-time-periods>
      </div>
      <div class="col-md-3 col-sm-6">
        <div class="col-title">Summary</div>
        <app-summary [selectedDevices]="selectedDevices" [deviceGroups]="deviceGroupsUpdated" 
                     [selectedProtocols]="selectedProtocols" [selectedTimePeriod]="selectedTimePeriod"
                     (clearAllClicked)="onClearAllClicked()">  
        </app-summary>
      </div>
    </div>

  `,
  styleUrls: ['./app.component.css'],
  providers: [DataService]
})
export class AppComponent implements OnInit {
  title = 'Cyberbit Task';
  public deviceGroups: DeviceGroup[];
  public protocols: Protocol[];
  public timePeriods: Time[];
  public selectedTimePeriod: Time;
  public selectedDevices = [];
  public selectedProtocols = [];
  public deviceGroupsUpdated = [];

  constructor(private dataService: DataService) {}

  ngOnInit() {
    this.dataService.getData().then(data => {
      this.deviceGroups = data.device_groups;
      this.protocols = data.protocols;
      this.timePeriods = data.times;
    });
  }

  onProtocolsChanged(event) {
    this.selectedProtocols = event.protocols;
  }

  removeSelectionsInArray(array) {
    for (let i = 0; i < array.length; i++) {
      array[i].checked = false;
    }
    return array;
  }

  onClearAllClicked() {
    this.removeSelectionsInArray(this.selectedDevices);
    this.removeSelectionsInArray(this.protocols);
    this.selectedProtocols = [];
    this.selectedDevices = [];
    this.deviceGroupsUpdated = [];
    for (let i = 0; i < this.deviceGroups.length; i++){
      this.deviceGroups[i]['checked'] = false;
    }
  }

  onDevicesCheckedChangedEvent(event) {
    this.selectedDevices = event.selectedDevices;
    this.deviceGroupsUpdated = event.deviceGroupsUpdated;
  }

  onRadioChangedEvent(event) {
    this.selectedTimePeriod = this.timePeriods[event.index];
  }
}
