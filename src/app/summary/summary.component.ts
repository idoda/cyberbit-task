import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {Device} from '../device';
import {Protocol} from '../protocol';
import {Time} from '../time';
import {DeviceGroup} from '../device-group';
import {Http} from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-summary',
  template: `
    <div class="section" [ngClass]="{'unSelected': selectedDevices?.length === 0}">
      <span class="bold">1</span>
      <span *ngIf="selectedDevices?.length === 0"> Selected Devices</span>
      <div class="group" *ngFor="let deviceGroup of deviceGroups;">      
        <div *ngFor="let device of deviceGroup.devices;">  
            <div  class="item" *ngIf="device.checked" (change)="updateSelectedDevices()">{{device.name}}</div>  
        </div>
      </div>
    </div>
    
    <div class="section" [ngClass]="{'unSelected': selectedProtocols?.length === 0}">
      <span class="bold">2</span>
      <span *ngIf="selectedProtocols?.length === 0"> Selected Protocols</span>
      <div class="item" *ngFor="let protocol of selectedProtocols;">
        {{protocol.name}}
      </div>
    </div>
    
    <div class="section" [ngClass]="{'unSelected': selectedTimePeriod == null}">
      <span class="bold">3</span>
      <span *ngIf="selectedTimePeriod == null">Time Period</span>
        <span  *ngIf="selectedTimePeriod != null">{{selectedTimePeriod.name}}</span>
    </div>
    
    <div class="actions">
      <span class="clear-btn" (click)="clearAllSelections()">Clear</span>
      <button [ngClass]="{'disabled': !isValid()}" (click)="sendHttpRequest()">
        <i class="fa fa-caret-right" aria-hidden="true"></i>
        <span>
          Start Learning</span>
      </button>
    </div>
  `,
  styleUrls: ['./summary.component.less']
})
export class SummaryComponent {
  @Input() deviceGroups: DeviceGroup[];
  @Input() selectedDevices: Device[];
  @Input() selectedProtocols: Protocol[];
  @Input() selectedTimePeriod: Time;
  @Output() clearAllClicked: EventEmitter<any> = new EventEmitter();

  constructor(private http: Http) { }

  clearAllSelections() {
    this.clearAllClicked.emit();
  }

  isValid() {
    return !(this.selectedProtocols.length === 0 && this.selectedDevices.length === 0 && this.selectedTimePeriod == null);
  }

  sendHttpRequest() {
    console.log('http request');
    let queryParams = '';
    let deviceIds = [];
    let protocolIds = [];
    this.selectedDevices.forEach((device) => deviceIds.push(device.id));
    this.selectedProtocols.forEach((protocol) => protocolIds.push(protocol.id));
    queryParams += 'devices=' + deviceIds.join(',') + '&protocols=' + protocolIds.join(',') + '&times=' + this.selectedTimePeriod.id;
    console.log(window.location.href + '?' + queryParams);
    this.http.get(window.location.href + '?' + queryParams)
      .map(res => res.text())
      .subscribe(
        data => console.log(data),
        err => console.log(err),
        () => console.log('Http Request Sent...')
      );
  }
}
