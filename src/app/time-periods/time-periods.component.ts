import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Time} from '../time';
import {DataService} from '../data.service';

@Component({
  selector: 'app-time-periods',
  template: `
    <ul>
      <li *ngFor="let time of times; let i = index;">
        <input name="times" type="radio" class="radio" (change)="radioCheckedChanged(i, $event)"> {{time.name}}
      </li>
    </ul>
  `,
  styleUrls: ['./time-periods.component.less']
})
export class TimePeriodsComponent {
  @Input() times: Time[];
  @Output() radioChangedEvent: EventEmitter<any> = new EventEmitter();

  constructor(private dataService: DataService) { }

  radioCheckedChanged(index, event) {
    this.radioChangedEvent.emit({index: index});
  }

}
