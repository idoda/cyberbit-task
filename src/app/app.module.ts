import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { DevicesComponent } from './devices/devices.component';
import { ProtocolsComponent } from './protocols/protocols.component';
import { TimePeriodsComponent } from './time-periods/time-periods.component';
import { SummaryComponent } from './summary/summary.component';
import { DeviceGroupsComponent } from './device-groups/device-groups.component';

@NgModule({
  declarations: [
    AppComponent,
    DevicesComponent,
    ProtocolsComponent,
    TimePeriodsComponent,
    SummaryComponent,
    DeviceGroupsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
