import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {Device} from '../device';

@Component({
  selector: 'app-devices',
  template: `
    <ul>
      <li class="device" *ngFor="let device of devices; let i = index;">
        <input type="checkbox" (change)="updateChecked(i, $event)" 
              [(ngModel)]="device.checked" class="form-check-inline">     
        {{device.name}}        
      </li>
    </ul>
    
  `,
  styleUrls: ['./devices.component.less']
})
export class DevicesComponent implements OnInit, OnChanges {
  @Input() devices: Device[];
  @Input() checkedInd = false;
  @Input() groupIndex;
  @Output() devicesCheckedChangedEvent: EventEmitter<any> = new EventEmitter();

  constructor() { }

  isAllDevicesChecked() {
    for (let i = 0; i < this.devices.length; i++) {
      if (this.devices[i]['checked'] === false) {
        return false;
      }
    }
    return true;
  }

  updateChecked(index, event) {
    this.devices[index]['checked'] = event.target.checked;
    this.devicesCheckedChangedEvent.emit(
      {
        bool: this.isAllDevicesChecked(),
        groupId: this.groupIndex,
        selectedDevices: this.devices.filter(device => device['checked'] === true)}
      );
  }

  ngOnInit() {
    for (let i = 0; i < this.devices.length; i++) {
      this.devices[i]['checked'] = false;
    }
  }

  ngOnChanges(changes: any) {
    if (changes.checkedInd.currentValue === false) {
      for (let i = 0; i < this.devices.length; i++) {
        this.devices[i]['checked'] = false;
        // this.updateChecked(i, {target: {checked: false}});
      }
    } else if (changes.checkedInd.currentValue === true) {
      for (let i = 0; i < this.devices.length; i++) {
        this.devices[i]['checked'] = true;
      }
    }
  }
}
