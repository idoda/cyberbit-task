import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { DATA } from '../ex_data';
import {DataService} from '../data.service';
import {DeviceGroup} from '../device-group';

@Component({
  selector: 'app-device-groups',
  template: `
      <div class="device-groups" *ngFor="let deviceGroup of deviceGroups; let i = index;">
      <div class="device-group" (click)="toggleDisplayGroup(i, $event)">
        <i *ngIf="!deviceGroup.isVisible" class="fa fa-caret-down" aria-hidden="true"></i>
        <i *ngIf="deviceGroup.isVisible" class="fa fa-caret-up" aria-hidden="true"></i>
        <input type="checkbox" class="vertical-align-middle" (change)="deviceGroupCheckedChanged(i, $event)"
               [(ngModel)]="deviceGroup.checked"> {{deviceGroup.name}}  
      </div>
      <app-devices [ngClass]="{'display-none': !deviceGroup.isVisible}" [groupIndex]="i" [checkedInd]="deviceGroup.checked" 
                   (devicesCheckedChangedEvent)="onDevicesCheckedChangedEvent($event)" [devices]="deviceGroup.devices">        
      </app-devices>
    </div>

  `,
  styleUrls: ['./device-groups.component.less']
})
export class DeviceGroupsComponent implements OnInit {
  @Input() deviceGroups: DeviceGroup[];
  @Output() devicesCheckedChangedEvent: EventEmitter<any> = new EventEmitter();

  constructor(private dataService: DataService) { }

  ngOnInit() {
    for (let i = 0; i < this.deviceGroups.length; i++) {
      this.deviceGroups[i]['checked'] = false;
    }
  }

  toggleDisplayGroup(index, event) {
    if (event.target.tagName === 'INPUT') {
      return;
    }
    if (this.deviceGroups[index]['isVisible'] == null) {
      this.deviceGroups[index]['isVisible'] = true;
    } else {
      this.deviceGroups[index]['isVisible'] = !this.deviceGroups[index]['isVisible'];
    }
  }

  deviceGroupCheckedChanged(i, event) {
    for (let j = 0; j < this.deviceGroups[i].devices.length; j++) {
      this.deviceGroups[i].devices[j].checked = event.target.checked;
    }
    this.devicesCheckedChangedEvent.emit({selectedDevices: this.deviceGroups[i].devices, deviceGroupsUpdated: this.deviceGroups});
  }

  onDevicesCheckedChangedEvent(event) {
    this.deviceGroups[event.groupId]['checked'] = event.bool;
    this.devicesCheckedChangedEvent.emit({selectedDevices: event.selectedDevices, deviceGroupsUpdated: this.deviceGroups});
  }
}
