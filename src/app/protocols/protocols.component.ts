import {Component, EventEmitter, Input, Output} from '@angular/core';
import {DataService} from '../data.service';
import {Protocol} from '../protocol';

@Component({
  selector: 'app-protocols',
  template: `    
   <ul *ngIf="protocols && protocols.length > 0">
     <li *ngFor="let protocol of protocols; let i = index;">
       <input type="checkbox" class="form-check-inline" [(ngModel)]="protocol.checked"
              (change)="updateProtocolChecked(i, $event)"> {{protocol.name}}  
     </li>
   </ul>
  `,
  styleUrls: ['./protocols.component.less']
})
export class ProtocolsComponent {
  @Input() protocols: Protocol[] = [];
  @Output() protocolsChanged: EventEmitter<any> = new EventEmitter();

  constructor(private dataService: DataService) { }

  updateProtocolChecked(index, event) {
    this.protocols[index]['checked'] = event.target.checked;
    this.protocolsChanged.emit({protocols: this.protocols.filter(protocol => protocol['checked'] === true)});
  }

}
