import { Injectable } from '@angular/core';
import {DATA} from './ex_data';
import {DeviceGroup} from './device-group';
import {Protocol} from './protocol';
import {Time} from './time';

@Injectable()
export class DataService {
  private data = DATA;

  constructor() { }

  getDeviceGroups(): Promise<DeviceGroup[]> {
    return Promise.resolve(DATA.device_groups);
  }

  getProtocols(): Promise<Protocol[]> {
    return Promise.resolve(DATA.protocols);
  }

  getTimes(): Promise<Time[]> {
    return Promise.resolve(DATA.times);
  }

  getData(): Promise<any> {
    return Promise.resolve(DATA);
  }

}
